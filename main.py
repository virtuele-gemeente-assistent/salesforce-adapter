import os
import logging
from fastapi import FastAPI, Request, HTTPException
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from scripts.session import is_redis_working
from scripts.sfapi import get_livechat_availability, get_opening_hours
from scripts.utils import extract_livechat_config, get_stats_activity
from scripts.message_handler import handle_message, handle_event
from scripts.session import get_redis_client, recreate_session_tasks

# Configuration
ENVIRONMENT = os.getenv("ENVIRONMENT")
ELASTIC_APM_SERVER_URL = os.getenv("ELASTIC_APM_SERVER_URL")
ELASTIC_APM_SECRET_TOKEN = os.getenv("ELASTIC_APM_SECRET_TOKEN")
SALESFORCE_ADAPTER_SENTRY_DSN = os.getenv("SALESFORCE_ADAPTER_SENTRY_DSN")

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

logger = logging.getLogger("main")

app = FastAPI()

# Configure Sentry for error monitoring if DSN is available
if SALESFORCE_ADAPTER_SENTRY_DSN:
    import sentry_sdk

    sentry_sdk.init(dsn=SALESFORCE_ADAPTER_SENTRY_DSN)

# Configure Elastic APM if server URL is available
if ELASTIC_APM_SERVER_URL:
    apm = make_apm_client({
        "ELASTIC_APM_SERVER_URL": ELASTIC_APM_SERVER_URL,
        "ELASTIC_APM_SECRET_TOKEN": ELASTIC_APM_SECRET_TOKEN,
        "SERVICE_NAME": f"Gem Camunda Cache {ENVIRONMENT}"
    })
    app.add_middleware(ElasticAPM, client=apm)

chat_listener_tasks = {} #TODO: use a better way to store the chat listener tasks
db_client = get_redis_client()

@app.on_event("startup")
async def startup_event():
    """Check if Redis is working on startup"""
    if not is_redis_working():
        raise RuntimeError("Redis is not working. Please check your Redis configuration.")
    
    else:
        logger.info("Redis is working")
        logger.info("Checking existing sessions")
        recreate_session_tasks(db_client, chat_listener_tasks)

    
@app.get('/')
def index():
    """Check if the service is up and running"""
    stats_result = get_stats_activity(db_client, chat_listener_tasks) #TODO: check all connections for all recipient_ids. See follow_status in def livechat(request: Request)
    return {'status': 'ok', 'stats': str(stats_result)}

@app.get('/{livechat_config}/livechat/')
def livechat(request: Request):
    """
    Check if the livechat is available
    params:
        livechat_config: the livechat configuration
    returns:
        a dict with the status of the livechat
    example return values:
    {"status": "ok"} # medewerkers beschikbaar gedurende de openingsuren V
    {"status": "not_available"} # medewerkers niet beschikbaar gedurende de openingsuren V
    {"status": "closed"} # buiten de openingsuren V
    {"status": "not_ok"} # livechat is gesloten of bezet V
    """

    stats_result = get_stats_activity(db_client, chat_listener_tasks)
    livechat_config = extract_livechat_config(request)
    livechat_availability = get_livechat_availability(livechat_config)
    is_open = get_opening_hours(livechat_config)

    # check if agents are available
    if livechat_availability:
        logger.info("status livechat: ok")
        return {'status': 'ok', 'stats': stats_result}

    # check if livechat is open and agents are not available
    elif is_open and not livechat_availability:
        logger.info("status livechat: not_available")
        return {'status': 'not_available', 'stats': stats_result}
    
    # check if livechat is closed
    elif not is_open:
        logger.info("status livechat: closed")
        return {'status': 'closed', 'stats': stats_result}
    
    else:
        logger.info("status livechat: not_ok")
        return {'status': 'not_ok', 'stats': stats_result}

@app.post('/{livechat_config}/livechat/')
async def livechat_post(request: Request):
    """
    This function handles incoming messages from the Gem chatbot.
    Args:
        request (Request): The request
    Returns:
        None
    """
    try:
        data = await request.json()
        sender = data.get('sender')
        livechat_config = extract_livechat_config(request)

        if 'message' in data:
            await handle_message(chat_listener_tasks, data, sender, livechat_config)

        elif 'event' in data:
            await handle_event(chat_listener_tasks, data, sender)

        else:
            raise HTTPException(status_code=400, detail=f'Invalid data for session "{sender}"')
    except Exception as error:
        raise HTTPException(status_code=500, detail=str(error)) from error