import os
import requests

# Use the correct environment variable name
DOCKER_CONTAINER_URL = os.environ.get("SALESFORCE_CONTAINER_URL", "http://localhost:8008")

def test_api_endpoint_returns_ok():
    # Make an API request to the container
    response = requests.get(f"{DOCKER_CONTAINER_URL}/")

    # Check if the response status code is 200 (OK)
    assert response.status_code == 200

    # Check if the response JSON content matches the expected data
    expected_data = {'status': 'ok'}
    assert response.json() == expected_data