fastapi==0.100.1
uvicorn[standard]
aiohttp==3.8.5
redis==4.6.0
elastic-apm==6.7.2
sentry-sdk[fastapi]==1.29.2
requests