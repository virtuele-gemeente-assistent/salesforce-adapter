import asyncio
import logging
import json
from scripts.sfapi import pull_messages
from scripts.gemapi import (
    post_agent_joined,
    post_text_message,
    post_button_message,
    pass_chat_to_gem,
    set_typing_on,
    set_typing_off,
    send_system_message
    )

from datetime import datetime

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

logger = logging.getLogger("sfchatlistener")

IDLE_TIME = 20 * 60 # 20 minutes

async def start_chat_listener(recipient_id, affinity, session_key, db_client, chat_listener_tasks):
    """
    This function listens for Salesforce chat events and sends them to the Gem chatbot.
    Args:
        recipient_id (str): The recipient_id
        affinity: The affinity
        session_key: The session key
    Returns:
        None
    """
    from scripts.session import delete_chat_task

    session_data = db_client.get(recipient_id)
    session_data = json.loads(session_data)
    livechat_config = session_data["livechat_config"]

    # Idle timer
    timer = None

    async def idle_timer(recipient_id):
        """
        This function is called when the idle timer expires.
        Args:
            recipient_id (str): The recipient_id of the Salesforce chat session
        Returns:
            None
        """
        await asyncio.sleep(IDLE_TIME)
        logger.warning("Idle timer expired for user %s", recipient_id)
        nonlocal continue_polling
        continue_polling = False

    async def start_timer():
        """
        This function starts the idle timer.
        Args:
            None
        Returns:
            None
        """
        nonlocal timer
        timer = asyncio.create_task(idle_timer(recipient_id))

    async def restart_timer():
        """
        This function restarts the idle timer.
        Args:
            None
        Returns:
            None
        """
        nonlocal timer
        if timer:
            timer.cancel()
        await start_timer()

    async def delete_timer():
        """
        This function deletes the idle timer.
        Args:
            None
        Returns:
            None
        """
        logger.info("delete_timer for user %s", recipient_id)
        nonlocal timer
        if timer:
            timer.cancel()
            timer = None

    start_chat_request = None
    continue_polling = True

    async def update_stats(db_client, recipient_id, status="active"):
        """
        This function updates the stats in Redis.
        Args:
            db_client: The Redis client
            recipient_id (str): The recipient_id
            status (str): The status
        Returns:
            None
        """
        existing_value = db_client.get(recipient_id)
        # Parse the existing value as JSON
        existing_value_dict = json.loads(existing_value)
        # Add the new key-value pair
        existing_value_dict['stats'] = status
        # Set the updated value back to Redis
        db_client.set(recipient_id, json.dumps(existing_value_dict))

    # Start long polling
    while continue_polling:
        try:
            result = await pull_messages(affinity, session_key, livechat_config)
            logger.info("result: %s for user %s", result, recipient_id)
            # check if session still exists
            session_exists = db_client.get(recipient_id)
            # if session exists, check if there are messages
            if session_exists:
                logger.info("Session exists for user %s", recipient_id)
                if result and result["messages"] and len(result["messages"]) > 0:
                    message = result["messages"][0]["message"]
                    message_type = result["messages"][0]["type"]
                    sequence = result.get("sequence")

                    if message_type == "ChatRequestSuccess":
                        logger.info("> Waiting for Salesforce agent to accept your chat request.")
                        start_chat_request = datetime.now()
                        await update_stats(db_client, recipient_id, "in_queue")
                        await send_system_message("livechat.queue.welcome", recipient_id)
                        
                    elif message_type == "ChatRequestFail":
                        logger.warning("> Failed to route Salesforce chat to agent with reason \"%s\".", message['reason'])
                        
                        if message['reason'] == 'Unavailable':
                            logger.info("> No agents available. Returning to Gem.")
                            await pass_chat_to_gem(recipient_id, "not_available", message.get('reason'))
                        else:
                            await pass_chat_to_gem(recipient_id, "error", message.get('reason'))
                            logger.info("> Error in chat request. Returning to Gem.")
                        
                        continue_polling = False

                    elif message_type == "ChatEstablished":
                        logger.info("> Salesforce agent accepted chat request. Listening for messages ...")
                        await update_stats(db_client, recipient_id, "active")
                        await post_agent_joined(recipient_id)
                        
                        # Start idle timer
                        await start_timer()

                    elif message_type in ["ChatMessage", "RichMessage"]:
                        #TODO: fix agent joined in router
                        #NOTE: artificial delay to ensure agent joined message is sent first
                        if sequence and sequence == 3:
                            logger.info("Message received with sequence 3..")
                            logger.info("Sleeping for 0.8 seconds...")
                            await asyncio.sleep(0.8)
                            logger.info("Waking up...")

                        if message_type == "RichMessage" and "items" in message and message["items"]:
                            message_subtype = message["type"]
                            if message_subtype in ["ChatWindowMenu", "ChatWindowButton"]:
                                buttons = [{"title": item["text"], "payload": item["text"]} for item in message["items"]]
                                await post_button_message('Maak uw keuze.', buttons, recipient_id)
                        else:
                            logger.info("> Salesforce agent send message")
                            await post_text_message(message["text"].replace('RICH_TEXT:', '').replace('/<\/?[^>]+(>|$)/g', ''), recipient_id)

                        # Restart idle timer
                        await restart_timer()

                    elif message_type == "ChatEnded" or message_type == "AgentDisconnect":
                        await update_stats(db_client, recipient_id, "cancelled")
                        logger.info("> Salesforce agent has ended the chat.")
                        await pass_chat_to_gem(recipient_id, "success")
                        continue_polling = False
                    
                    elif message_type == "ChatTransferred":
                        logger.info("> Salesforce agent has transferred the chat.")
                        await send_system_message("livechat.queue.handover", recipient_id)

                    elif message_type == "AgentTyping":
                        logger.info("> Salesforce agent started typing.")
                        await set_typing_on(recipient_id)

                    elif message_type == "AgentNotTyping":
                        logger.info("> Salesforce agent stopped typing.")
                        await set_typing_off(recipient_id)
                
                    # if the queue position has changed and the diff time from ChatRequestSuccess is more than 5 seconds
                    elif message_type == "QueueUpdate" and start_chat_request:
                        diff_time_from_start = (datetime.now()-start_chat_request).total_seconds()
                        logger.info("diff_time_from_start in seconds: %s", diff_time_from_start)
                        if diff_time_from_start > 5:
                            logger.info("> The queue position has changed for session %s", recipient_id)

                            queue_position = message.get('position')
                            logger.info("current queue position: %s", queue_position)
                            if queue_position == 0:
                                await send_system_message("livechat.queue.busy", recipient_id)

                            elif queue_position == 1:
                                await send_system_message("livechat.queue.busy_position1", recipient_id, position=queue_position)
                            
                            else:
                                await send_system_message("livechat.queue.busy_position", recipient_id, position=queue_position)

                    else:
                        logger.warning("Unhandled Salesforce chat event %s", result)

                else:
                    logger.info("> Waiting for Salesforce agent to send a message ...")
            else:
                logger.info("> This session does not exist anymore or has been closed.")
                continue_polling = False

        except Exception as error:
            logger.error("Something went wrong. %s", error)
            continue_polling = False

    # if continue_polling is False, delete the idle timer
    if not continue_polling:
        await delete_timer()

    # End process
    logger.info("End Salesforce chat listener for user %s", recipient_id)
    delete_chat_task(recipient_id, db_client, chat_listener_tasks)
