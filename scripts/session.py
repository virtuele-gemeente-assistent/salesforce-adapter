import os
import time
import redis
import logging
import asyncio
import json

from scripts.sfchatlistener import start_chat_listener


logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

logger = logging.getLogger("session")

redis_host = os.environ.get("REDIS_HOST", "redis")
redis_port = int(os.environ.get("REDIS_PORT", 6379))
redis_db = int(os.environ.get("REDIS_DB", 12))

def is_redis_working(retry_attempts=5, retry_sleep=2):
    """
    Check if Redis is working by performing a simple SET/GET operation.
    Args:
        retry_attempts (int): Number of retry attempts in case of failure (default: 5).
        retry_sleep (int): Sleep time in seconds between retry attempts (default: 2).
    Returns:
        bool: True if Redis is working, False otherwise.
    """
    try:
        redis_client = redis.StrictRedis(
            host=redis_host,
            port=redis_port,
            db=redis_db,
            decode_responses=True
            )
        
        test_key = "12345"
        test_value = "56789"

        for attempt in range(retry_attempts + 1):
            # Try setting the test key
            redis_client.set(test_key, test_value)
            # Try getting the test key
            get_key = str(redis_client.get(test_key))
            # Check if the key is successfully set
            if get_key == test_value:
                # Delete the test key after the successful test
                redis_client.delete(test_key)
                return True
            
            # Sleep before the next retry attempt
            time.sleep(retry_sleep)

            if attempt < retry_attempts:
                logger.warning("Attempt %s/%s: Redis is not responding. Retrying in %s seconds...", attempt, retry_attempts, retry_sleep)

        return False

    except Exception as error:
        logger.error("Error while checking if Redis is working: %s", error)
        return False
    
def get_redis_client():
    """
    This function returns a Redis client
    Args:
        None
    Returns:
        Redis client
    """
    return redis.StrictRedis(
        host=redis_host,
        port=redis_port,
        db=redis_db,
        decode_responses=True
        )

def create_chat_task(sender, affinity, session_key, db_client, chat_listener_tasks):
    """
    This function creates a chat listener task.
    args:
        sender: the sender of the message
        affinity: the affinity of the chat session
        session_key: the session key of the chat session
        db_client: the Redis client
        chat_listener_tasks: the dict of chat listener tasks
    returns:
        None
    """
    chat_listener_task = asyncio.create_task(
        start_chat_listener(
            sender,
            affinity,
            session_key,
            db_client,
            chat_listener_tasks,
        )
    )
    
    return chat_listener_task
    
def delete_chat_task(recipient_id, db_client, chat_listener_tasks):
    """
    This function deletes a chat listener task.
    Args:
        recipient_id (str): The recipient_id
        db_client: The Redis client
        chat_listener_tasks: The dict of chat listener tasks
    Returns:
        None
    """
    db_client.delete(recipient_id)

    chat_listener_task = chat_listener_tasks.get(recipient_id)
    if chat_listener_task:
        chat_listener_task.cancel()
        logger.info("Chat listener task cancelled for session %s", recipient_id)
        del chat_listener_tasks[recipient_id]

def recreate_session_tasks(db_client, chat_listener_tasks):
    """
    Recreate the chat listener tasks for existing sessions in Redis.
    Args:
        db_client (Redis): The Redis client
        chat_listener_tasks (dict): The chat listener tasks
    Returns:
        None
    """
    sessions = db_client.keys()
    if sessions:
        for session in sessions:
            if session_data := db_client.get(session):
                session_data = json.loads(session_data)
                logger.info("Session data found for session %s", session)

                chat_task = create_chat_task(
                    session,
                    session_data['affinity'],
                    session_data['sessionKey'],
                    db_client,
                    chat_listener_tasks
                )

                chat_listener_tasks[session] = chat_task
                logger.info("Chat listener task created for session %s", session)
    else:
        logger.info("No existing sessions found")