import base64
from fastapi import HTTPException
import json

import logging


logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

logger = logging.getLogger("utils")

def extract_livechat_config(request):
    """
    Extract livechat config from URL
    params:
        request: FastAPI request object
    return:
        dict with livechat config
    """
    try:
        # extract url params
        livechat_config = request.path_params['livechat_config']

        if livechat_config:
            livechat_config = livechat_config.split('-')
            livechat_config = {
                "SF_APEXREST_URL": base64.b64decode(livechat_config[0]).decode("utf-8"),
                'SF_REST_URL': base64.b64decode(livechat_config[1]).decode("utf-8"),
                'CHAT_BUTTONID': livechat_config[2],
                'CHAT_DEPLOYMENTID': livechat_config[3],
                'CHAT_ORGANISATIONID': livechat_config[4]
            }
            return livechat_config
        else:
            logger.error('Error extracting livechat_config: No livechat_config found in URL')
            raise HTTPException(
                status_code=400,
                detail='No livechat_config found in URL'
            )
    except Exception as error:
        logger.error('Error extracting livechat_config: %s', str(error))
        raise HTTPException(
            status_code=400,
            detail=f'Error extracting livechat_config: {str(error)}'
        ) from error

def get_stats_activity(db_client, chat_listener_tasks) -> dict:
    """
    Get stats about the activity of the chat listener tasks
    params:
        db_client: redis client
        chat_listener_tasks: dict of chat_listener_tasks
    return:
        dict with stats
    """
    follow_stats = {
        "active": [],   # sender_ids, medewerker heeft chat geaccepteerd
        "in_queue": [],  # sender_ids in wachtrij, medewerker heeft chat nog niet geaccepteerd
        "idle": [],      # sneder_ids niet actief, niet in de queue en niet cancelled
        "cancelled": [], # sender_ids. chat is geannuleerd door user_disconnect of wanneer een medewerker een chat afsluit
        "coroutine tasks": len(chat_listener_tasks),  # sender_ids van de chat_listener_tasks
    }

    for recipient_id in chat_listener_tasks:
        try:
            # get redis object for recipient_id
            redis_object = db_client.get(recipient_id)

            if redis_object:
                redis_object = json.loads(redis_object)

                # Check if "stats" field is present
                if "stats" in redis_object:
                    stats_value = redis_object["stats"]

                    if stats_value:
                        follow_stats[stats_value].append(recipient_id)
                else:
                    logging.warning("Missing 'stats' field for recipient_id:  %s", recipient_id)

        except Exception as error:
            logging.error(
                "Error processing Error: %s", error,
                extra={"recipient_id": recipient_id}
                )

    return follow_stats