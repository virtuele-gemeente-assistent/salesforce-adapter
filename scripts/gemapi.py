import os
import logging
from typing import List
import aiohttp

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

logger = logging.getLogger("gemapi")

ROUTER_URL = os.environ.get('ROUTER_URL')
ROUTER_API_KEY = os.environ.get('ROUTER_API_KEY')
ENVIRONMENT_URL = os.environ.get('ENVIRONMENT_URL','http://environment:8000')

AVATAR_URL = 'https://mijn.test.virtuele-gemeente-assistent.nl/media/gemeente-tilburg_NrgmESk.png'
SENDER_NAME = 'Gemeente Tilburg'

async def get_system_messages(message_id=None):
    """
    This function gets the system messages.
    args:
        message_id: the message ID
    returns:
        the system messages
    """
    url = f"{ENVIRONMENT_URL}/system-messages"
    
    if message_id:
        url += f"?id={message_id}"

    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(url) as response:
                return await response.json()
        except aiohttp.ClientResponseError as error:
            logger.error("HTTP error: %s - %s", error.status, error.message)
            raise Exception("HTTP error: %s - %s", error.status, error.message) from error
        except aiohttp.ClientError as error:
            logger.error("Client error: %s", error)
            raise Exception("Client error: %s", error) from error

async def send_system_message(message_id: str, recipient_id: str, position=None) -> None:
    """
    This function sends a system message to the Gem chatbot.
    Args:
        message_id (str): The message ID
    Returns:
        None
    example of get_system_messages:
        {"id": "livechat.queue.welcome", "message": "Een moment geduld. Je staat in de wachtrij."},
        {"id": "livechat.queue.busy", "message": "Een moment geduld. Er komt zo een medewerker bij je."},
        {"id": "livechat.queue.busy_position1", "message": "Een moment geduld. Er is 1 persoon voor je in de wachtrij."}
        {"id": "livechat.queue.busy_position", "message": "Een moment geduld. Er zijn {} personen voor je in de wachtrij."}
    """
    try:
        response = await get_system_messages(message_id)
        system_message = response.get("message")

        if position is not None and position != 0:
            system_message = system_message.format(position)

        if system_message:
            await post_message(system_message, recipient_id)
        else:
            # TODO: handle error
            pass
    except Exception as error:
        logger.error("Error in send_system_message: %s", error)

async def make_router_request(payload: dict) -> dict:
    """
    This function makes a request to the router.
    args:
        payload: the payload to send to the router
    returns:
        the response from the router
    """
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(
                ROUTER_URL,
                json=payload,
                headers={"X-API-Key": ROUTER_API_KEY}
                ) as response:
                return await parse_response_data(response)
    except Exception as error:
        logger.error("Error in router request: %s", error)
        return None
            
async def parse_response_data(response: aiohttp.ClientResponse) -> dict:
    """
    This function parses the response data.
    args:
        response: the response from the router
    returns:
        the response data
    """
    content_type = response.headers.get('Content-Type', '').lower()
    if 'application/json' in content_type:
        return await response.json()
    elif 'text/plain' in content_type:
        return await response.text()
    else:
        return await response.read()

async def pass_chat_to_gem(recipient_id: str, status: str, reason=None) -> dict:
    """
    This function passes the chat to GEM.
    args:
        recipient_id: the recipient ID
        status: the status with
        reason: the reason for the status
        example: success, not_available, error

    returns:
        the response from the router
    """

    
    payload = {
        "recipient_id": recipient_id,
        "custom": {
            "deescalate": True,
            "status": status,
        }
    }
    if status == 'error' and reason is not None:
        payload["custom"]["error"] = reason

    return await make_router_request(payload)

async def post_agent_joined(recipient_id: str) -> dict:
    """
    This function sends an agent joined event to the router.
    args:
        recipient_id: the recipient ID
    returns:
        the response from the router
    """
    payload = {
        "recipient_id": recipient_id,
        "custom": {
            "agent_joined": True
        }
    }
    return await make_router_request(payload)

async def post_message(
        text: str or None,
        recipient_id: str,
        buttons: List[dict] = None,
        typing_event: str = None
        ) -> dict:
    """
    This function posts a message to the router.
    args:
        text: the text to send
        recipient_id: the recipient ID
        buttons: the buttons to send
        typing_event: the typing event
    returns:
        the response from the router
    """

    payload = {"recipient_id": str(recipient_id)}
    metadata = {
        "metadata": {
            "avatarUrl": str(AVATAR_URL),
            "senderName": str(SENDER_NAME)
            }
        }

    if text:
        payload.update({"text": str(text)})
        payload.update(metadata)

    if buttons:
        payload.update({"text": str(text)})
        payload.update(metadata)
        payload["buttons"] = buttons

    if typing_event:
        payload["custom"] = {"event": typing_event}

    return await make_router_request(payload)


async def post_text_message(text: str, recipient_id: str) -> dict:
    """
    This function posts a text message to the router.
    args:
        text: the text to send
        recipient_id: the recipient ID
    returns:
        the response from the router
    """
    return await post_message(text, recipient_id)


async def post_button_message(
        text: str,
        buttons: List[dict],
        recipient_id: str
        ) -> dict:
    """
    This function posts a button message to the router.
    args:
        text: the text to send
        buttons: the buttons to send
        recipient_id: the recipient ID
    returns:
        the response from the router
    """
    return await post_message(text, recipient_id, buttons)

async def set_typing_on(recipient_id: str) -> dict:
    """
    This function sets the typing indicator to on.
    args:
        recipient_id: the recipient ID
    returns:
        the response from the router
    """
    return await post_message(None, recipient_id, typing_event="typing_on")


async def set_typing_off(recipient_id: str) -> dict:
    """
    This function sets the typing indicator to off.
    args:
        recipient_id: the recipient ID
    returns:
        the response from the router
    """
    return await post_message(None, recipient_id, typing_event="typing_off")